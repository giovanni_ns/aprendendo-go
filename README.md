# Aprendendo Go

Olá a todos que estão visualizando este repositório. É um prazer tê-los aqui!

Este repositório tem como objetivo compartilhar meu planejamento e linha de aprendizado da linguagem de programação Go. Além disso, pretendo fornecer uma base de conhecimento central para aprendizagem.

## Recursos para aprender Go

Aqui estão alguns sites e páginas que estou utilizando para criar este repositório de "Aprendendo Go":

- [Documentação oficial do Go](https://golang.org/)
- [Tour pela linguagem Go](https://tour.golang.org/welcome/1)
- [Golang.org Wiki](https://github.com/golang/go/wiki)
- [Effective Go](https://golang.org/doc/effective_go.html)
- [Curso "Aprenda Go"](https://www.learn-go.dev/)
- [Roadmap Go Developer](https://roadmap.sh/golang)
- [W3Schools](https://www.w3schools.com/go/)
- [Go by Example](https://gobyexample.com/)

## Ferramentas utilizadas

- Editor de texto: [Visual Studio Code](https://code.visualstudio.com/) \
  Extensões recomendadas:
  - [Go](https://marketplace.visualstudio.com/items?itemName=golang.Go)
  - [vscode-icons](https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons)
  - [markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
- Cliente HTTP: [Insomnia](https://insomnia.rest/download)
- Contêiner: [Docker](https://www.docker.com/)

## Desenvolvimento Full-Stack?

Aqui também estarei trazendo o desenvolvimento fullstack para criar visuais e comunicação com o backend em Go. Ainda estou indeciso e irei atualizar sobre a tecnologia utilizada futuramente para esse tipo de desenvolvimento.

Estou com grande dúvida entre React, Vue ou Angular (os famosos "três cavaleiros" do Frontend).

## Configuração da minha máquina

Estou utilizando um `Samsung Book NP550XDA-KH3BR` com as seguintes configurações:

|||
| --- | --- |
| OS | Ubuntu 22.04 (LTS) |
| RAM | 16 GB |
| CPU | i5 1135g7 |
| GPU | Gráficos Intel® Iris® Xᵉ |
| NVME | 512 GB |
| HDD | 1 TB |
| Tela | 15,6' (1920x1080) |
