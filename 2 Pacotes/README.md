# Pacotes

Cada programa Go é composto de pacotes.

Programas começam rodando pelo pacote main.

Este programa está usando os pacotes com caminhos de importação `"fmt"` e `"math/rand"`.

Por convenção, o nome do pacote é o mesmo que o último elemento do caminho de importação. Por exemplo, o
pacote `"math/rand"` compreende arquivos que começam com package rand.

**Nota**: o ambiente em que esses programas são executados é determinístico, então rand.Intn sempre retornará o mesmo
número.

## Importações

Este grupo de códigos em parênteses da importação, é a declaração de importação "consignada".

``` go
import (
    "fmt"
    "math"
)
```

Você também pode escrever várias declarações de importação assim:

``` go
import "fmt"
import "math"
```

## Nomes exportados

Em Go, um nome é exportado se ele começa com uma letra maiúscula. Por exemplo, `Pizza` é um nome exportado, assim
como `Pi`, que é exportado do pacote math.

`pizza` e `pi` não começam com uma letra maiúscula, logo eles não são exportados.

Ao importar um pacote, você pode referenciar apenas seus nomes exportados. Todos os nomes "não exportados" não são
acessíveis de fora do pacote.

Execute o código. Observe a mensagem de erro.

Para corrigir o erro, renomeie `math.pi` para `math.Pi` e tente novamente.

## Funções

A função pode ter zero ou mais argumentos.

Neste exemplo, adicione (`add`) dois parâmetros do tipo `int`.

Observe que o tipo vem após o nome da variável.

``` go
func add(x int, y int) int {
    return x + y
}
```

### Funções continuação

Quando dois ou mais parâmetros nomeados consecutivos de uma função compartilham o mesmo tipo, você pode omitir o tipo de
todos menos o último. Neste exemplo, vamos encurtar.

``` go
func add(x, y int) int {
    return x + y
}
```

### Resultados Múltiplos

Uma função pode retornar qualquer número de resultados.

A função `swap` retorna duas strings.

``` go
func swap(x, y string) (string, string) {
    return y, x
}
```

## Valores nomeados de retorno

Valores de retorno de Go podem ser nomeados e agirem apenas como variáveis.

Esses nomes devem ser usados para documentar o significado dos valores de retorno.

A declaração `return` sem argumentos retorna os valores atuais dos resultados. Isto é conhecido como um retorno "limpo".

Instruções de retorno limpas devem ser usadas apenas em funções curtas, como no exemplo mostrado aqui. Elas podem
prejudicar a legibilidade em funções mais longas.

``` go
func split(sum int) (x, y int) {
    x = sum * 4 / 9
    y = sum - x
    return
}
```

## Variáveis

A instrução `var` declara uma lista de variáveis, como em listas de argumentos de função, o tipo é o último passado.

A declaração `var` pode estar num pacote ou a nível de função. Nós vemos ambos neste exemplo.

### Variáveis com inicializadores

A declaração var pode incluir inicializadores, uma por variável.

Se um inicializador está presente, o tipo pode ser omitido; a variável terá o tipo do inicializador.

### Declarações curtas de variáveis

Dentro de uma função a instrução de atribuição curta `:=` pode ser utilizada em lugar de uma declaração var com o tipo
implícito.

Fora de uma função cada estrutura começa com uma palavra-chave (`var`, `func`, e assim por diante) e não é possível usar
o `:=`.

``` go
func main() {
    var i, j int = 1, 2
    k := 3
    c, python, java := true, false, "no!"

    fmt.Println(i, j, k, c, python, java)
}
```

## Tipos básicos

Os tipos básicos de Go são:

    bool

    string

    int  int8  int16  int32  int64
    uint uint8 uint16 uint32 uint64 uintptr

    byte // pseudônimo para uint8

    rune // pseudônimo para int32
    // representa um ponto de código Unicode

    float32 float64

    complex64 complex128

O exemplo mostra vários tipos de variáveis e também que as declarações de variáveis podem ser "construídas" em blocos,
como com as declarações de importação.

Os tipos `int`, `uint` e `uintptr` são geralmente de 32 bits em sistemas de 32 bits e 64 bits em sistemas de 64 bits.
Quando você precisar de um valor inteiro deverá
usar `int`, a menos que tenha um motivo específico para usar um tipo de inteiro com tamanho especificado ou sem sinal.

### Conversões de tipo

 A expressão T(v) converte o valor v para o tipo T.

Algumas conversões numéricas:

``` go
var i int = 42
var f float64 = float64(i)
var u uint = uint(f)
```

Ou, de uma forma mais simples:

``` go
i := 42
f := float64(i)
u := uint(f)
```

Diferente de C, em Go atribuição entre os itens de tipo diferente requer uma conversão explícita. Tente remover as conversões float64 ou uint no exemplo e veja o que acontece.

## Constantes

Constantes são declaradas como variáveis, mas com a palavra-chave `const`.

Constantes podem ser seqüências de caracteres, booleanos, ou valores numéricos.

Constantes não podem ser declaradas usando a sintaxe `:=`.

``` go
package main

import "fmt"

const Pi = 3.14

func main() {
    const World = "世界"
    fmt.Println("Hello", World)
    fmt.Println("Happy", Pi, "Day")

    const Truth = true
    fmt.Println("Go rules?", Truth)
}
```
