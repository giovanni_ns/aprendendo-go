package main

import (
	"fmt"
	"math"
	"math/rand"
)

// Funções continuação
func add(x, y int) int {
	return x + y
}

// Resultados Múltiplos
func swap(x, y string) (string, string) {
	return y, x
}

// Valores nomeados de retorno
func split(sum int) (x, y int) {
	x = sum * 4 / 9
	y = sum - x
	return // retorna multiplos
}

func conversoesTipo() {
	var x, y int = 3, 4
	var f float64 = math.Sqrt(float64(x*x + y*y))
	var z uint = uint(f)
	fmt.Println(x, y, z)
}

func constExemplo() {
	const World = "Mundo"
	fmt.Println("Olá,", World)
}

// Váriaveis

var c, python, java bool = false, true, false // Igual função, se forem do mesmo tipo é necessário somente tipar o ultimo

func main() {
	fmt.Println("Me numero favorito é", rand.Intn(10))
	fmt.Printf("Você tem %g problemas.\n", math.Sqrt(7)) // Como contatenar atravez do %
	fmt.Println(math.Pi)

	fmt.Println(add(17, 24))

	a, b := swap("Hello", "world")
	fmt.Println(a, b)

	fmt.Println(split(17))

	// Váriaveis com inicializadores
	var i int = 1

	fmt.Println(i, c, python, java)

	// Declarações curtas de variáveis
	j := "Pão"
	k := 3
	ruby, pearl, java := true, false, "no!"

	fmt.Println(i, j, k, ruby, pearl, java)

	conversoesTipo()
	constExemplo()
}
